import React, { Component } from "react";


class Todos extends Component {
    deleteTodo = id => {
        this.props.onDelete(id);
    }
    checkChecked = (id, event) => {
        let checkedVal = event.target.checked;
        this.props.onChecked(checkedVal, id);
    }
    render() {
        let todosItem;
        if (this.props.todos) {
            todosItem = this.props.todos.map(todo => {
                let clsName = todo.chk ? "checked-todo" : "unchecked-todo";
                return (
                    <div className="todo-item"  key={todo.title} >
                        <input type="checkbox" onChange={this.checkChecked.bind(this, todo._id)}/>
                        <strong className ={clsName}> {todo.title} </strong> 
                        <a className="delete-button" href="#" onClick={this.deleteTodo.bind(this, todo._id)}>X</a>
                    </div>
                );
            })
        }
        return (
            <div className="todos-list">
                {todosItem}
            </div>
        )
    }
}
export default Todos;

