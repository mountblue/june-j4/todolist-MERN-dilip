import React from 'react'

export default function TodoInput(props) {

    let handleSubmit = (event) => {
        if (event.key === "Enter") {
            let inputVal = event.target.value;
            event.target.value = "";
            if (inputVal.trim()) {
                props.todoInputValue(inputVal);
            } else {
                alert("plz enter your todo..");
            }
        }
    }
    
    let handleButton = (event) =>{
        let inputVal = document.getElementById("input").value;
        document.getElementById("input").value ="";
        console.log(inputVal)
        if (inputVal.trim()) {
            props.todoInputValue(inputVal);
        } else {
            alert("plz enter your todo..");
        }

    }
    return (
        <div className="main-container">
            <h3>Todo-List</h3>
            <div className="text-field-container">
                <input id="input" className="input" onKeyPress={handleSubmit} placeholder="Enter todo here...... and press enter" type="text" autoFocus/>
                {/* <input type="submit" value="Submit" onClick={handleButton} /> */}
            </div>
        </div>
    )

}
