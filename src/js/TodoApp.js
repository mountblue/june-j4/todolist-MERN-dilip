import React, { Component } from "react";
import { render } from "react-dom";
import ""../css/style.css";
import uuid from "uuid";
import TodoItems from "../components/TodoItems";
import TodoInput from ""../components/TodoInput";


class ToDoList extends Component {
    constructor() {
        super();
        this.state = {
            allTodos: []
        }
    }

    getTodos = () =>  {
        fetch('http://localhost:3003/api/todos')
            .then(response => response.json())
            .then(data => this.setState({ allTodos: data }));
    }

    componentDidMount() {
        this.getTodos();
    }

    handleDeleteTodo = (id) => {
          return fetch("http://localhost:3003/api/todos/" + id, {
              method: "DELETE",
          })
          .then(this.componentDidMount())
          .catch(error => console.error(`Error in deleting =\n`, error));
      };

    handleCheckTodo = (checkedVal, id) => {
        let updateTodo = {
            _id: id,
            chk: checkedVal
        }
        return fetch('http://localhost:3003/api/todos', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(updateTodo),
        })
            .then(response => response.json())
            .then(this.componentDidMount())
            .catch(error => console.error(`Error in Updating =\n`, error));
    };

    storeTodo = (inputVal) => {
        let newTodo = {
            _id: uuid.v4(),
            title: inputVal,
            chk: false
        }
        return fetch('http://localhost:3003/api/todos', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(newTodo)
        })
            .then(response => response.json())
            .then(this.componentDidMount())
            .catch(error => console.error(`Error in Addinng =\n`, error));
    };

    render() {
        return (
            <div>
                <TodoInput todoInputValue={this.storeTodo} />
                <TodoItems todos={this.state.allTodos}
                    onChecked={this.handleCheckTodo}
                    onDelete={this.handleDeleteTodo} />
            </div>
        )
    }
}
export default ToDoList
